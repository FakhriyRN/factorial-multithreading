#with python3
#Septian Nabilah Salsabillah 4210171003
#Yusril CHalif Arrahman 4210171004
#Fakhriy Ramadhan Nahidha 4210171027
#Oryza Ummi Sholichah   4210171020
import threading

number = 0
result = 1

factorial = 10
threads = 4

class Factorial:
    def run(self, index):
        global number, factorial, result
        while number < factorial / threads * index:
            number += 1
            result *= number
            print("Thread-%d %d" % (index, number))

fac = Factorial()
for i in range(threads):
    thread = threading.Thread(target=fac.run, args=(i + 1, ))
    thread.start()
    thread.join()
    
print("Final Result : ", result)
